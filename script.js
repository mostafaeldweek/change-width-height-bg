let container = document.querySelector(".container");

let x = setInterval(() => {
  container.style.backgroundColor = `rgb(${Math.random() * 256},${
    Math.random() * 256
  }, ${Math.random() * 256})`;
  container.style.width = parseInt(container.style.width || 0) + 10 + "px";
  container.style.height = parseInt(container.style.height || 0) + 10 + "px";
  console.log(container.style.height);
}, 1000);

/*
if you need to end the setInterval after 8 or any numers times
setTimeout(() => {
   clearInterval(x);
 }, 8000);

 */
